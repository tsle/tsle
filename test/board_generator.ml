open Geometry
open Side
open Piece

let two_pawns (direction:Geometry.direction) = 
  let empty_board = Board.chessboard 8
  and pos1 = (4,4)
  and white_pawn = Piece.wpawn
  and black_pawn = Piece.bpawn in
  let board1 = Board.occupy white_pawn pos1 empty_board
  and pos2 = Geometry.next 1 pos1 direction in
  let board2 = Board.occupy black_pawn pos2 board1 in
  (board2, white_pawn, black_pawn, pos1, pos2)
;;

let castling (side:Side.side) (kside:bool) =
  let empty_board = Board.chessboard 8
  and (king, rook) = if side = Side.classic_white
    then (Piece.wking, Piece.wrook)
    else (Piece.bking, Piece.brook)
  and rook_pos = Piece.classic_position ~kside:kside (side.color) Rook
  and king_pos = Piece.classic_position side.color King in
  let kboard = Board.occupy king king_pos empty_board in
  let castling_board = Board.occupy rook rook_pos kboard in
  (castling_board, king, rook, king_pos, rook_pos, kside)
;;

let with_opposing_piece_in_front pos distance side kind board =
  let front_pos = Geometry.next distance pos side.direction in
  let opposing_piece = Piece.piece kind (Side.invert side) in
  Board.occupy opposing_piece front_pos board
;;

let with_king_checked_by_queen castling_situation =
  let (board, king, rook, king_pos, rook_pos, kside) = castling_situation in
  let qboard = with_opposing_piece_in_front king_pos 5 king.side Piece.Queen board in
  (qboard, king, rook, king_pos, rook_pos, kside)
;;

let with_rook_attacked_by_rook castling_situation =
  let (board, king, rook, king_pos, rook_pos, kside) = castling_situation in
  let rboard = with_opposing_piece_in_front rook_pos 1 king.side Piece.Rook board in
  (rboard, king, rook, king_pos, rook_pos, kside)
;;

let with_king_trajectory_attacked_by_rook n castling_situation =
  let (board, king, rook, king_pos, rook_pos, kside) = castling_situation in
  let king_pos_plus_n = Geometry.next n king_pos (direction king_pos rook_pos) in
  let rboard = with_opposing_piece_in_front king_pos_plus_n 1 king.side Piece.Rook board in
  (rboard, king, rook, king_pos, rook_pos, kside)
;;

let with_moved_king castling_situation =
  let (board, king, rook, king_pos, rook_pos, kside) = castling_situation in
  let moved_king = { king with moved = true } in
  let board = Board.occupy moved_king king_pos board in
  (board, moved_king, rook, king_pos, rook_pos, kside)
;;

let with_moved_rook castling_situation =
  let (board, king, rook, king_pos, rook_pos, kside) = castling_situation in
  let moved_rook = { rook with moved = true } in
  let board = Board.occupy moved_rook rook_pos board in
  (board, king, moved_rook, king_pos, rook_pos, kside)
;;

let castling_moved_king (side:Side.side) (kside:bool) =
  with_moved_king (castling side kside)
;;

let castling_moved_rook (side:Side.side) (kside:bool) =
  with_moved_rook (castling side kside)
;;

let castling_all_moved (side:Side.side) (kside:bool) =
  with_moved_rook (castling_moved_king side kside)
;;

let castling_king_checked_by_queen (side:Side.side) (kside:bool) =
  with_king_checked_by_queen (castling side kside)
;;

let castling_rook_attacked_by_rook (side:Side.side) (kside:bool) =
  with_rook_attacked_by_rook (castling side kside)
;;

let castling_king_trajectory_attacked_by_rook (n:int) (side:Side.side) (kside:bool) =
  with_king_trajectory_attacked_by_rook n (castling side kside)
;;
