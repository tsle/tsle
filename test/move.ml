open Side
open Piece
open Random_generator
open Side_generator
open Board_generator

let pawn_legal_taking (direction:Geometry.direction) =
  let (board, _, _, pos1, pos2) = two_pawns direction in
  let side:Side.side = { Side.color = Side.White; Side.direction = Geometry.N } in
(*  Board.print board;*)
  Piece.legal_taking_pawn side pos1 pos2 = Piece.Legal_taking
;;

let pawn_illegal_en_passant (direction:Geometry.direction) =
    let (board, white_pawn, black_pawn, pos1, pos2) = two_pawns direction in
    let new_pos2 = Geometry.next 1 pos2 Geometry.S in
(*  Board.print board;*)
  try
    let square1 = Board.find_square board pos1
    and square2 = Board.find_square board pos2
    and new_square2 = Board.find_square board new_pos2 in
    match Board.legal_move board black_pawn square2 new_square2 with
    | Board.Legal actions ->
      begin
	let board = Board.apply board actions in
	let square2 = Board.find_square board pos2 in
	match Board.legal_move board white_pawn square1 square2 with
	| Board.Legal _ -> false
	| _ -> true
      end
    | _ -> false
  with Not_found -> false
;;

let pawn_legal_move (direction:Geometry.direction) =
  let (board, white_pawn, _, pos1, pos2) = two_pawns direction in
(*  Board.print board;*)
  try
    let square1 = Board.find_square board pos1
    and square2 = Board.find_square board pos2 in
    match Board.legal_move board white_pawn square1 square2 with
    | Board.Legal _ -> true
    | _ -> false
  with Not_found -> false
;;

let castling_generator = app (app (pure castling) random_side) QCheck.Arbitrary.bool;;
let castling_moved_king_generator = app (app (pure castling_moved_king) random_side) QCheck.Arbitrary.bool;;
let castling_moved_rook_generator = app (app (pure castling_moved_rook) random_side) QCheck.Arbitrary.bool;;
let castling_all_moved_generator = app (app (pure castling_all_moved) random_side) QCheck.Arbitrary.bool;;
let castling_king_checked_by_queen_generator = app (app (pure castling_king_checked_by_queen) random_side) QCheck.Arbitrary.bool;;
let castling_rook_attacked_by_rook_generator = app (app (pure castling_rook_attacked_by_rook) random_side) QCheck.Arbitrary.bool;;
let castling_king_trajectory_attacked_by_rook_generator n = app (app (pure (castling_king_trajectory_attacked_by_rook n)) random_side) QCheck.Arbitrary.bool;;

let legal_castling castling expected =
  let (board, king, _, king_pos, _, kside) = castling in
(* Board.print board; *)
(*  print_endline ((Geometry.string_of_pos king_pos) ^ ", " ^ (string_of_bool kside));*)
  let king_dest_pos = Piece.king_castling_dest kside king.side.color in
  try
    let king_square = Board.find_square board king_pos
    and king_dest = Board.find_square board king_dest_pos in
    match Board.legal_move board king king_square king_dest with
    | Board.Legal _ -> expected
    | _ -> not expected
  with Not_found -> not expected
;;
