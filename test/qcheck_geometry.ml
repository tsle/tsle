let invert_test1 = QCheck.mk_test ~n:1000
  ~name:"Geometry invert function is involutive on directions"
  QCheck.Arbitrary.(among Geometry.directions)
  (fun d -> Geometry.invert (Geometry.invert d) = d);;

let invert_test2 = QCheck.mk_test ~n:1000
    ~name:"Geometry invert function is involutive on vectors (unsat preconditions not filtered)"
    ~pp:Geometry.string_of_pos_par
    QCheck.Arbitrary.(pair small_int small_int)
    (fun v ->
      QCheck.Prop.assume (try let f = fun x -> true in f (Geometry.dir_of_vect v) with
      | Division_by_zero -> false
      | Invalid_argument m -> false
      | _ -> print_string ((Geometry.string_of_pos_par v) ^ "\n"); true);
      let d = (Geometry.dir_of_vect v) in
      Geometry.vect_of_dir (Geometry.invert (Geometry.invert d)) = Geometry.simplify v);;

let invert_test3 = QCheck.mk_test ~n:1000
    ~name:"Geometry invert function is involutive on vectors (unsat preconditions filtered)"
    ~pp:Geometry.string_of_pos_par
    (Random_generator.succeed (QCheck.Arbitrary.(pair small_int small_int))
	|> Random_generator.guard (fun v ->
	  try let _ = Geometry.dir_of_vect v in true with
	  | Division_by_zero -> false
	  | Invalid_argument _ -> false
	  | _ -> true)
	|> Random_generator.backtrack)
    (fun v ->
      let d = (Geometry.dir_of_vect v) in
      Geometry.vect_of_dir (Geometry.invert (Geometry.invert d)) = Geometry.simplify v);;

QCheck.run_tests [invert_test1; invert_test2; invert_test3];;
