open QCheck.Arbitrary
open Side_generator
open Geometry_generator
open Move

let pawn_taking_pawn_test1 = QCheck.mk_test ~n:100
  ~name:"Pawn taking North is correct"
  random_north
  (fun d -> Move.pawn_legal_taking d)
;;

let pawn_taking_pawn_test2 = QCheck.mk_test ~n:100
  ~name:"A pawn can take a North pawn"
  random_north
  (fun d -> Move.pawn_legal_move d)
;;

let pawn_taking_pawn_test3 = QCheck.mk_test ~n:100
  ~name:"A pawn cannot take en passant a North pawn illegaly"
  random_north
  (fun d -> Move.pawn_illegal_en_passant d)
;;

let king_castling_test1 = QCheck.mk_test ~n:100
  ~name:"The king can be castled in a pure simple castling position."
  castling_generator
  (fun castling -> Move.legal_castling castling true)
;;

let king_castling_test2 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if it has moved."
  castling_moved_king_generator
  (fun castling -> Move.legal_castling castling false)
;;

let king_castling_test3 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if the target rook has moved."
  castling_moved_rook_generator
  (fun castling -> Move.legal_castling castling false)
;;

let king_castling_test4 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if it has moved as well as the target rook."
  castling_all_moved_generator
  (fun castling -> Move.legal_castling castling false)
;;

let king_castling_test5 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if it is in check."
  castling_king_checked_by_queen_generator
  (fun castling -> Move.legal_castling castling false)
;;

let king_castling_test6 = QCheck.mk_test ~n:100
  ~name:"The king can be castled even if the rook is under attack."
  castling_rook_attacked_by_rook_generator
  (fun castling -> Move.legal_castling castling true)
;;

let king_castling_test7 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if its trajectory square 1 is under attack."
  (castling_king_trajectory_attacked_by_rook_generator 1)
  (fun castling -> Move.legal_castling castling false)
;;

let king_castling_test8 = QCheck.mk_test ~n:100
  ~name:"The king cannot be castled if its trajectory square 2 is under attack."
  (castling_king_trajectory_attacked_by_rook_generator 2)
  (fun castling -> Move.legal_castling castling false)
;;

QCheck.run_tests [pawn_taking_pawn_test1;
		  pawn_taking_pawn_test2;
		  pawn_taking_pawn_test3;
		  king_castling_test1;
		  king_castling_test2;
		  king_castling_test3;
		  king_castling_test4;
		  king_castling_test5;
		  king_castling_test6;
		  king_castling_test7;
		  king_castling_test8]
;;
