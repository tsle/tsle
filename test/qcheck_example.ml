let test = QCheck.mk_test ~n:1000
    ~name:"list_rev_is_involutive"
    QCheck.Arbitrary.(list small_int)
    (fun l -> List.rev (List.rev l) = l);;
QCheck.run test;;
