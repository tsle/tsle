open QCheck.Arbitrary
open Geometry

let random_direction = among directions;;
let random_north = among [NE; NW];;
