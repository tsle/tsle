type game = {
  players : Player.player list;
  queue : Player.player list;
  board : Board.board;
}
val update_queue : game -> game
val next : game -> game
val current_player : game -> Player.player
