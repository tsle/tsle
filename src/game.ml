open Player
open Board

(* A game is defined by: *)
type game = {
  players: player list; (* a ordered list of players *)
  queue: player list; (* the queue of remaining players this turn *)
  board: board (* the chessboard *)
}

let update_queue game =
  match game.queue with
    | [] -> { game with queue = game.players }
    | _ -> game
;;

let next game =
  { game with queue = List.tl game.queue }
;;

let current_player game =
  List.hd game.queue
;;
