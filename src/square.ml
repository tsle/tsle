open Side
open Geometry
open Piece

type state = Occupied of piece | Empty;;

(* A square know its position,
 * this may be modified since we use arrays for encoding boards *)
type square = {
  pos: position;
  color: color;
  state: state
}

exception Square_not_found of string;;

let col square = fst square.pos;;
let row square = snd square.pos;;

let string_of_square square =
  match square with
    | {color = Black; state = Occupied(p)} ->
      "[" ^ string_of_piece p ^ "]"
    | {color = Black; state = Empty} -> "[ ]"
    | {color = White; state = Occupied(p)} ->
      "(" ^ string_of_piece p ^ ")"
    | {color = White; state = Empty} -> "( )"
;;

let debug_square square =
  let pos_string = string_of_pos square.pos in
  match square.color with
    | Black -> "[" ^ pos_string  ^ "]"
    | White -> "(" ^ pos_string ^ ")"
;;

(* Returns an empty square of the given color *)
let empty position color =
  { pos = position; color = color; state = Empty }
;;

(* Returns a copy of the square, occupied by the given piece *)
let occupy square piece =
  { square with state = Occupied(piece) }
;;

(* Instantiates the square with respect to classical chess rules
 * given its position *)
let classic_init square =
  match square.pos with
    | (1, 1) -> occupy square Piece.wrook   | (1, 8) -> occupy square Piece.brook
    | (2, 1) -> occupy square Piece.wknight | (2, 8) -> occupy square Piece.bknight
    | (3, 1) -> occupy square Piece.wbishop | (3, 8) -> occupy square Piece.bbishop
    | (4, 1) -> occupy square Piece.wqueen  | (4, 8) -> occupy square Piece.bqueen
    | (5, 1) -> occupy square Piece.wking   | (5, 8) -> occupy square Piece.bking
    | (6, 1) -> occupy square Piece.wbishop | (6, 8) -> occupy square Piece.bbishop
    | (7, 1) -> occupy square Piece.wknight | (7, 8) -> occupy square Piece.bknight
    | (8, 1) -> occupy square Piece.wrook   | (8, 8) -> occupy square Piece.brook
    | (_, 2) -> occupy square Piece.wpawn
    | (_, 7) -> occupy square Piece.bpawn
    | _ -> square
;;

(* Returns true if there is a piece of the given color on this square *)
let occupied colorOption square =
  match square.state, colorOption with
  | Empty, _ -> false
  | Occupied(p), Some color -> p.side.color = color
  | _ -> true
;;

(* Returns the piece on this square,
 * or Not_found if the square is empty. *)
let piece square color =
  match square.state with
    | Empty -> raise Not_found
    | Occupied(p) ->
      if p.side.color = color then p else raise Not_found
;;

let empty_or_opposing square color =
  match square.state with
    | Occupied(p) when p.side.color = color -> false
    | _ -> true
;;

let opposing square color =
  match square.state with
  | Occupied(p) when p.side.color <> color -> true
  | _ -> false
;;

(* Same function as List.exists, adapted to a square,
 * i.e. tests the given predicate on each piece of the square *)
let exists predicate square =
  match square.state with
    | Occupied(p) -> predicate p
    | _ -> false
;;

(* If the square is occupied by a pawn of this color, resets it. *)
let reset_pawn color square =
  match square.state with
  | Occupied p when p.side.color = color && p.kind = pawn true ->
    occupy square { p with kind = pawn_init }
  | _ -> square
;;

(* If the square is occupied by a piece of this color, resets its moved flag *)
let reset_moved_flag color square =
  match square.state with
  | Occupied p when p.side.color = color ->
    occupy square { p with moved = false }
  | _ -> square
;;
