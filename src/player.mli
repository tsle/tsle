type player = {
  lang : Language.language;
  name : string;
  side : Side.side;
  qside_castled : bool;
  kside_castled : bool;
}
val player :
  Language.language -> string -> Side.color -> Geometry.direction -> player
val ( ^$ ) : char -> string -> string
val random_name : unit -> string
val name : ?color:Side.color -> unit -> string
