open Geometry

type color = Black | White;;
type side = { color: color; direction: direction };;

let classic_white = { color = White; direction = N };;
let classic_black = { color = Black; direction = S };;

let string_of_color c =
  match c with
  | Black -> "noir"
  | White -> "blanc"
;;

let letter_of_color c =
  String.sub (string_of_color c) 0 1
;;

let string_of_side s =
  (string_of_color s.color) ^ ":" ^ (string_of_dir s.direction)
;;

let invert_color c =
  match c with
    | Black -> White
    | White -> Black
;;

let invert s =
  { color = invert_color s.color; direction = Geometry.invert s.direction }
;;

let backward side =
  Geometry.invert side.direction
;;

let forward_diags side =
  Geometry.diags side.direction
;;

let backward_diags side =
  Geometry.diags (Geometry.invert side.direction)
;;
