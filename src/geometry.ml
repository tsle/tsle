type position = int * int;;
type direction = N | NE | E | SE | S | SW | W | NW;;

let directions = [N; NE; E; SE; S; SW; W; NW];;

let string_of_dir d =
  match d with
  | N -> "North"
  | NE -> "North-East"
  | E  -> "East"
  | SE -> "South-East"
  | S  -> "South"
  | SW -> "South-West"
  | W  -> "West"
  | NW -> "North-West"
;;

let string_of_pos pos =
  string_of_int (fst pos) ^ ", " ^ string_of_int (snd pos)
;;

let string_of_pos_par pos =
  "(" ^ string_of_pos pos ^ ")"
;;

let equal pos1 pos2 =
  fst pos1 = fst pos2 && snd pos1 = snd pos2
;;

let direction_vector orig dest =
  (fst dest - fst orig, snd dest - snd orig)
;;

let vect_of_dir dir =
  match dir with
    | N  -> (0, 1)
    | NE -> (1, 1)
    | E  -> (1, 0)
    | SE -> (1, -1)
    | S  -> (0, -1)
    | SW -> (-1, -1)
    | W  -> (-1, 0)
    | NW -> (-1, 1)
;;

let rec pgcd u v =
  if v = 0 then
    u
  else
    pgcd v (u mod v)
;;

let simplify vector =
  let (abs, ord) = vector in
  let pgcd = Pervasives.abs (pgcd abs ord) in
  (abs / pgcd, ord / pgcd)
;;

let dir_of_vect vector =
  let vect = simplify vector in
  match vect with
    | (0, 1)   -> N
    | (1, 1)   -> NE
    | (1, 0)   -> E
    | (1, -1)  -> SE
    | (0, -1)  -> S
    | (-1, -1) -> SW
    | (-1, 0)  -> W
    | (-1, 1)  -> NW
    | _ -> invalid_arg ("Vector " ^ string_of_pos_par vector
			^ " once simplified " ^ string_of_pos_par vect
			^ " does not match with a cardinal point.")
;;

let direction orig dest =
  dir_of_vect (direction_vector orig dest)
;;

let invert d =
  let (i, j) = vect_of_dir d in
  dir_of_vect (-i, -j)
;;

let next n pos dir =
  let (col, row) = pos in
  match dir with
    | N  -> (col    , row + n)
    | NE -> (col + n, row + n)
    | E  -> (col + n, row)
    | SE -> (col + n, row - n)
    | S  -> (col    , row - n)
    | SW -> (col - n, row - n)
    | W  -> (col - n, row)
    | NW -> (col - n, row + n)
;;

let diags d =
  match d with
  | N -> [NW; NE]
  | E -> [NE; SE]
  | S -> [SE; SW]
  | W -> [SW; NW]
  | _ -> invalid_arg ("One should not need to compute diagonals for " ^ string_of_dir d)
;;
