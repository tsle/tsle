open Language
open Side

type player = {
  lang: language; (* the selected language *)
  name: string;
  side: side;
  qside_castled: bool;
  kside_castled: bool
}

let player lang name color direction : player =
  let side = {color = color; direction = direction} in
  {lang = lang; name = name; side = side ; qside_castled = true; kside_castled = true}
;;

Random.self_init ();;

let (^$) c s =
  s ^ Char.escaped c
;;

let random_name () =
  let rec naming n name =
    if n <= 0 then name
    else
      let random_c = char_of_int (Random.int 256) in
      naming (n-1) (name ^ (Char.escaped random_c))
  in
  naming 5 ""
;;

let name ?color:(c=White) () =
  match read_line () with
    | "" -> string_of_color c
    | "random" -> random_name ()
    | new_name -> new_name
;;
