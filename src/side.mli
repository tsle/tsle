type color = Black | White
type side = { color : color; direction : Geometry.direction; }
val classic_white : side
val classic_black : side
val string_of_color : color -> string
val letter_of_color : color -> string
val string_of_side : side -> string
val invert_color : color -> color
val invert : side -> side
val backward : side -> Geometry.direction
val forward_diags : side -> Geometry.direction list
val backward_diags : side -> Geometry.direction list
