type state = Occupied of Piece.piece | Empty
type square = { pos : Geometry.position; color : Side.color; state : state; }
exception Square_not_found of string
val col : square -> int
val row : square -> int
val string_of_square : square -> string
val debug_square : square -> string
val empty : Geometry.position -> Side.color -> square
val occupy : square -> Piece.piece -> square
val classic_init : square -> square
val occupied : Side.color option -> square -> bool
val piece : square -> Side.color -> Piece.piece
val empty_or_opposing : square -> Side.color -> bool
val opposing : square -> Side.color -> bool
val exists : (Piece.piece -> bool) -> square -> bool
val reset_pawn : Side.color -> square -> square
