type pawn = { just_jumped_two_squares : bool; }
type kind = King | Queen | Rook | Knight | Bishop | Pawn of pawn
type piece = { kind : kind; side : Side.side; moved : bool; }
type piece_legality =
    Legal_moving
  | Legal_pawn_two_squares_jump
  | Legal_taking
  | Legal_qside_castling
  | Legal_kside_castling
  | Illegal
val string_of_legality : piece_legality -> string
val string_of_kind : kind -> string
val letter_of_kind : kind -> string
val string_of_piece : piece -> string
val piece : kind -> Side.side -> piece
val pawn : bool -> kind
val pawn_init : kind
val wking : piece
val bking : piece
val wqueen : piece
val bqueen : piece
val wrook : piece
val brook : piece
val wknight : piece
val bknight : piece
val wbishop : piece
val bbishop : piece
val wpawn : piece
val bpawn : piece
val classic_position : ?kside:bool -> Side.color -> kind -> int * int
val king_castling_dest : bool -> Side.color -> int * int
val rook_castling_dest : bool -> Side.color -> int * int
val opposing : piece -> Side.side -> bool
val reset : piece -> piece
val legal_vectors : kind -> Geometry.position list
val legal_king_castling_vectors : (int * int) list
val legal_moving_of_bool : bool -> piece_legality
val legal_moving_generic : piece -> Geometry.position -> piece_legality
val legal_moving_one_shot :
  piece -> Geometry.position -> Geometry.position -> piece_legality
val legal_moving_infinite :
  piece -> Geometry.position -> Geometry.position -> piece_legality
val legal_king_castling : Geometry.position -> bool
val legal_moving_king :
  piece -> Geometry.position -> Geometry.position -> piece_legality
val legal_moving_pawn :
  piece -> Geometry.position -> Geometry.position -> piece_legality
val legal_taking_pawn :
  Side.side -> Geometry.position -> Geometry.position -> piece_legality
val promotion : piece -> piece
