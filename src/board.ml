open Geometry
open Side
open Piece
open Square

(* Representation type of a board.
 * It is a matrix, an array (rows numbered from n to 1)
 * of columns (arrays of squares numbered from 1 to n) :
 * [|[|(1, n)  ; (2, n)  ; ...; (n, n)  |];
 *   [|(1, n-1); (2, n-1); ...; (n, n-1)|];
 *   ...
 *   [|(1, 1)  ; (2, 1)  ; ...; (n, 1)  |]|]
 * where we use the notation (c, r) for selecting a square with
 * c is the column number and
 * r is the row number.
 * Numbering begins at 1, as in chess game. *)
type board = (square array) array;;

let row_of_index i n = n - i;;

(* Converts a position (column, row) into a position
 * in a matrix of size n. *)
let index_of_pos (c, r) n =
  (n - r, c - 1)
;;

(* Converts a position in a matrix of size n
 * into a position (column, row). *)
let pos_of_index (i, j) n =
  (j + 1, n - i)
;;

(* Returns a board debug string *)
let debug_board board =
  for i = 0 to Array.length board - 1 do
    for j = 0 to Array.length board.(i) - 1 do
      let (col, row) = board.(i).(j).pos in
      match board.(i).(j).color with
        | White -> Printf.fprintf stdout "(%d, %d) " col row
        | Black -> Printf.fprintf stdout "[%d, %d] " col row
    done;
    Printf.fprintf stdout "\n"
  done;
;;

let print board =
  let print_line () =
    Printf.fprintf stdout "\n     ";
    for i = 0 to Array.length board - 1 do
      Printf.fprintf stdout "+----";
    done;
    Printf.fprintf stdout "+\n";
  in
  let print_alphabet () =
    Printf.fprintf stdout "\n   ";
    for i = 0 to Array.length board - 1 do
      (* Do not create board with size bigger than 190 ! *)
      Printf.fprintf stdout "    %c" (Char.chr (i + Char.code 'A'));
    done;
    Printf.fprintf stdout "\n\n";
  in
  for i = 0 to Array.length board - 1 do
    print_line ();
    Printf.fprintf stdout "  %d  |" (row_of_index i (Array.length board));
    for j = 0 to Array.length board.(i) - 1 do
      let square = board.(i).(j) in
      match square.state, square.color with
	| Empty, White -> Printf.fprintf stdout "    |"
	| Empty, Black -> Printf.fprintf stdout "|  ||"
	| (Occupied p), White -> Printf.fprintf stdout " %s |" (string_of_piece p)
	| (Occupied p), Black -> Printf.fprintf stdout "|%s||" (string_of_piece p)
    done
  done;
  print_line ();
  print_alphabet ()
;;

(* Returns a chessboard (square board) of size n. *)
let chessboard n =
  let init_square = {pos = (0, 0); color = White; state = Empty} in
  let board = Array.make_matrix n n init_square in
  for row = n downto 1 do
    for col = 1 to n do
      let (i, j) = index_of_pos (col, row) n
      and nil_mod = ((row + col) mod 2 = 0) in
      board.(i).(j) <-
        { pos = (col, row);
          color = if nil_mod then Black else White;
          state = Empty }
    done
  done;
  board
;;

(* Same function as Array.map, adapted to a board,
 * i.e. applies the given function to each square of the board. *)
let map f board =
  Array.map (Array.map f) board
;;

(* Same function as List.exists, adapted to a board,
 * i.e. tests the given predicate on each square of the board. *)
let exists predicate board =
  let n = Array.length board in
  let rec scan row col =
    let (i, j) = index_of_pos (col, row) n in
    let square = board.(i).(j) in
    if predicate square then
      true
    else
      if row = 1 && col = n then false
      else if col = n then scan (row - 1) 1
      else scan row (col + 1)
  in scan n 1
;;

(* Returns a list of the squares which satisfy the given predicate *)
let filter predicate board =
  let n = Array.length board in
  let rec scan row col squares =
    let (i, j) = index_of_pos (col, row) n in
    let square = board.(i).(j) in
    let squares =
      if predicate square then
	(square :: squares)
      else squares
    in if row = 1 && col = n then squares
      else if col = n then scan (row - 1) 1 squares
      else scan row (col + 1) squares
  in scan n 1 []
;;

(* Returns a classic chessboard (size 8)
 * in the classic initial configuration. *)
let classic_init () =
  map Square.classic_init (chessboard 8)
;;

(* Returns the square of the board which is located at the given position,
 * or Not_found if there is no square at this position. *)
let find_square board pos =
  let (i, j) = index_of_pos pos (Array.length board) in
  board.(i).(j)
;;

(* Returns the occupied squares of the board *)
let occupied_squares colorOption board =
  filter (Square.occupied colorOption) board
;;

let obstacle board (orig:position) (dest:position) vect =
  let dir = dir_of_vect vect in
  let next pos = find_square board (next 1 pos dir) in
  let rec test square =
    if square.pos = dest then false
    else if not (square.state = Empty) then true
    else test (next square.pos)
  in test (next orig)
;;

type violation =
  Occupied_destination of piece
| Blocked_way of piece
| Friendly_fire of piece
| Illegal_moving of piece
| Illegal_taking of piece
| Nothing_to_kill of piece
| Too_late_en_passant of piece
| Wrong_en_passant_target of piece
| King_checked of piece
| King_trajectory_attacked of square
| Rook_not_in_position_for_castling of bool
| Rook_has_moved of bool
| No_square_at_rook_castling_destination of position
;;

type move_legality =
  Legal of (board -> board) list
| Illegal of violation list
;;

let legal_moving (piece:piece) (orig:position) (dest:position) =
  match piece.kind with
  | King -> legal_moving_king piece orig dest
  | Knight -> legal_moving_one_shot piece orig dest
  | Pawn _ -> legal_moving_pawn piece orig dest
  | _ -> legal_moving_infinite piece orig dest
;;

let reset_pawns_jump_flags color board =
  map (reset_pawn color) board
;;

let occupy piece (pos:position) board =
  let n = Array.length board in
  let (i, j) = index_of_pos pos n in
  board.(i).(j) <- { board.(i).(j) with state = Occupied(piece) }; board
;;

let free (pos:position) board =
  let n = Array.length board in
  let (i, j) = index_of_pos pos n in
  board.(i).(j) <- { board.(i).(j) with state = Empty }; board
;;

let move piece (orig:position) (dest:position) board =
  let board = free orig board in
  let board = reset_pawns_jump_flags piece.side.color board in
  let piece = reset piece in
  occupy { piece with moved = true } dest board
;;

let pawn_two_squares_jump piece (orig:position) (dest:position) board =
  match piece.kind with
  | Pawn _ ->
    let board = move piece orig dest board in
    occupy { piece with kind = pawn true } dest board
  | _ -> failwith "Only a pawn can do the two squares jump." (* FIXME *)
;;

(* TODO: use a standard list function instead ? *)
let list_of_violations violation_options =
  let rec loop violations v_options =
    match v_options with
    | [] -> violations
    | Some(violation) :: rem_v_options -> loop (violation::violations) rem_v_options
    | _ :: rem_v_options -> loop violations rem_v_options
  in loop [] violation_options
;;

let legal_attack board piece (orig:square) (dest:square) =
  let piece_legal_moving = (legal_moving piece orig.pos dest.pos) = Legal_moving in
  match piece.kind with
  | King | Knight -> piece_legal_moving (* Legal_moving => not a castling *)
  | Queen | Rook | Bishop ->
    let dir_vect = simplify (direction_vector orig.pos dest.pos) in
    piece_legal_moving && not (obstacle board orig.pos dest.pos dir_vect)
  | Pawn _ -> (legal_taking_pawn piece.side orig.pos dest.pos) = Legal_taking
;;

let check board square color =
  let candidate_squares = List.filter (fun s ->
    not (Geometry.equal s.pos square.pos))
    (occupied_squares (Some (Side.invert_color color)) board)
  in
  (* Returns true if the candidate piece is attacking the tested square *)
  let check_piece candidate_square candidate_piece =
    legal_attack board candidate_piece candidate_square square
  in List.exists (fun s -> Square.exists (check_piece s) s) candidate_squares
;;

let rec legal_castling board king ~kside (orig:square) (dest:square) =
  let dir_vect = simplify (direction_vector orig.pos dest.pos) in
  if obstacle board orig.pos dest.pos dir_vect then
    Illegal [Blocked_way king]
  else
    let king_castling_direction = Geometry.direction orig.pos dest.pos in
    let square1 = find_square board (Geometry.next 1 orig.pos king_castling_direction)
    and square2 = find_square board (Geometry.next 2 orig.pos king_castling_direction) in
    if check board orig king.side.color then
      Illegal [King_checked king]
    else if check board square1 king.side.color then
      Illegal [King_trajectory_attacked square1]
    else if check board square2 king.side.color then
      Illegal [King_trajectory_attacked square2]
    else
      let color = king.side.color in
      let rook_pos = classic_position ~kside:kside color Rook in
      try
	let rook_sqr = find_square board rook_pos in
	let rook_sqr_content = Square.piece rook_sqr color in
	match rook_sqr_content.kind, rook_sqr_content.moved with
	| Rook, false ->
	  let rook_dest_pos = rook_castling_dest kside color in
	  begin
	    try
	      let rook_dest_sqr = find_square board rook_dest_pos in
	      match legal_move board rook_sqr_content rook_sqr rook_dest_sqr with
	      | Legal rook_move -> Legal ((move king orig.pos dest.pos) :: rook_move)
	      | violations -> violations
	    with Not_found -> Illegal [No_square_at_rook_castling_destination rook_dest_pos]
	  end
	| Rook, true -> Illegal [Rook_has_moved kside]
	| _ -> Illegal [Rook_not_in_position_for_castling kside]
      with Not_found -> Illegal [Rook_not_in_position_for_castling kside]

and legal_move board piece (orig:square) (dest:square) =
  let piece_legal_moving = legal_moving piece orig.pos dest.pos in
  let violating_moving =
    match piece_legal_moving with
    | Illegal -> Some(Illegal_moving piece)
    | _ -> None
  and violating_dest =
    match empty_or_opposing dest piece.side.color with
    | true -> None
    | false -> Some(Occupied_destination piece) in
  let n = Array.length board in
  match piece.kind with
  | King ->
    begin match [violating_moving; violating_dest] with
    | [None; None] ->
      begin match piece_legal_moving with
      | Legal_kside_castling -> legal_castling board piece ~kside:true orig dest
      | Legal_qside_castling -> legal_castling board piece ~kside:false orig dest
      | _ -> Legal [move piece orig.pos dest.pos]
      end
    | violation_options -> Illegal (list_of_violations violation_options)
    end
  | Knight ->
    begin
      match [violating_moving; violating_dest] with
      | [None; None] -> Legal [move piece orig.pos dest.pos]
      | violation_options -> Illegal (list_of_violations violation_options)
    end
  | Queen | Rook | Bishop ->
    begin match violating_moving with
    | None ->
      begin
	let dir_vect = simplify (direction_vector orig.pos dest.pos) in
	let obstruction =
	  match obstacle board orig.pos dest.pos dir_vect with
	  | false -> None
	  | true -> Some(Blocked_way piece)
	in
	match [obstruction; violating_dest] with
	| [None; None] -> Legal [move piece orig.pos dest.pos]
	| violation_options -> Illegal (list_of_violations violation_options)
      end
    | Some violation -> Illegal [violation]
    end
  | Pawn _ ->
    begin
      let empty_dest = dest.state = Empty
      and legal_taking = legal_taking_pawn piece.side orig.pos dest.pos
      and opposing = Square.opposing dest piece.side.color in
      begin
	if (legal_taking != Illegal) && empty_dest then
	  try
	    let en_passant_target_square =
	      find_square board (next 1 dest.pos (backward piece.side))
	    in match en_passant_target_square.state with
	    | Occupied(p) when p.side.color <> piece.side.color ->
	      begin
		match p.kind with
		| Pawn(target) when target.just_jumped_two_squares ->
		  Legal [move piece orig.pos dest.pos;
			 free en_passant_target_square.pos]
		| Pawn _ -> Illegal [Too_late_en_passant piece]
		| _ -> Illegal [Wrong_en_passant_target piece]
	      end
	    | _ -> Illegal [Nothing_to_kill piece]
	  with
	    Not_found -> raise (Square_not_found "Case cible de la prise en passant.")
	else
	  let (_, r2) = dest.pos in
	  let violating_dest =
	    match empty_dest with
	    | true -> None
	    | false -> Some(Occupied_destination piece)
	  and violating_taking =
	    match legal_taking with
	    | Illegal -> Some(Illegal_taking piece)
	    | _ -> None
	  and violating_target =
	    match opposing with
	    | true -> None
	    | false -> Some(Nothing_to_kill piece)
	  in match [violating_moving; violating_dest; violating_taking; violating_target] with
	  | None :: None :: _ when (r2 = n || r2 = 1) -> (* legal moving, legal destination, and promotion *)
	    Legal [move (promotion piece) orig.pos dest.pos]
	  | None :: None :: _ when (piece_legal_moving = Legal_pawn_two_squares_jump) -> (* legal moving, legal destination, and two squares jump *)
	    Legal [pawn_two_squares_jump piece orig.pos dest.pos]
	  | None :: None :: _ (* any legal moving and legal destination *)
	  | _ :: _ :: None :: [None] -> Legal [move piece orig.pos dest.pos] (* or any legal taking and legal target *)
	  | violation_options -> Illegal (list_of_violations violation_options)
      end
    end
;;

(* TODO: replace with standard list library function *)
let rec apply board actions =
  match actions with
  | [] -> board
  | action :: rem_actions ->
    let board = action board in
    apply board rem_actions
;;
