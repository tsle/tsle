open Language;;
open Side;;
open Board;;
open Player;;
open Game;;
open Piece;;

let new_game () =
  let () = print_string "Nom du joueur blanc: "
  in
  let name = Player.name ~color:Side.White () in
  let p1 = Player.player Fr name Side.White Geometry.N in
  let () = print_string "Nom du joueur noir: " in
  let name = Player.name ~color:Side.Black () in
  let p2 = Player.player Fr name Side.Black Geometry.S in
  let game = { 
    players = [p1; p2];
    queue = [];
    board = Board.classic_init ()
  }
  in game
in

let select () =
  try
    let () = print_string "
Choisissez une case:
- colonne (1-8): "
    in
    let col = read_int () in
    let () = print_string "- rangée (1-8): "
    in
    let row = read_int ()
    in (col, row)
  with
  | Failure("int_of_string") -> raise (Invalid_argument "not a number")
in

let () = print_string "
-----------------------
Tempête sur l'Échiquier
-----------------------

"
in

let rec move game () =
  let () = Board.print game.board in
  let game = Game.update_queue game in
  let player = Game.current_player game in
  let board = game.board in
  let () = print_string (player.name ^ " joue:") in
  try
    let orig = select () in
    let orig_square = Board.find_square board orig in
    let selected_piece = Square.piece orig_square player.side.color in
    let dest = select () in
    let dest_square = Board.find_square board dest in
    let legal_move = Board.legal_move board selected_piece orig_square dest_square in
    let king = selected_piece.kind = Piece.King in
    match legal_move with
    | Legal actions when ((king && not (Board.check board dest_square selected_piece.side.color))
			  || not king) ->
      let board = Board.apply board actions in
      move (next { game with board = board }) ()
    | Legal _ -> print_string "Le roi ne peut se mettre en échec.\n"; move game ()
    | Illegal violations ->
      List.iter (fun violation -> match violation with
      | Occupied_destination piece -> print_string "Déplacement impossible: la case est déjà occupée.\n"
      | Blocked_way piece -> print_string "Trajectoire bloquée.\n"
      | Friendly_fire piece -> print_string "La case est occupée par une pièce amie.\n"
      | Illegal_moving piece -> print_string "Schéma de déplacement illégal.\n"
      | Illegal_taking piece -> print_string "Schéma de prise illégale.\n"
      | Nothing_to_kill piece -> print_string "Prise impossible: la case est libre ou occupée par une pièce amie.\n"
      | Too_late_en_passant piece -> print_string "Le pion cible ne peut plus être pris en passant.\n"
      | Wrong_en_passant_target piece -> print_string "La pièce cible ne peut être prise en passant.\n"
      | King_checked piece -> print_string "Impossible: le roi est en échec.\n"
      | King_trajectory_attacked square -> print_string "Le roi ne peut pas roquer en passant par une case attaquée.\n"
      | Rook_not_in_position_for_castling kside ->
	let castling_string = if kside then "Petit" else "Grand" in
	print_string (castling_string ^ " roque impossible: la tour n'est pas en position.\n")
      | Rook_has_moved kside ->
	let castling_string = if kside then "Petit" else "Grand" in
	print_string (castling_string ^ " roque impossible: la tour a été déplacée durant la partie.\n")
      | No_square_at_rook_castling_destination rook_dest ->
	print_string "Roque impossible: case de destination de la tour inexistante.\n") violations;
      print_string "\nImpossible! Veuillez recommencer.\n\n"; move game ()
  with
  | Invalid_argument "not a number" ->
    print_string "\nPas un nombre! Veuillez recommencer.\n\n";
    move game ()
  | Not_found
  | Invalid_argument "index out of bounds" ->
    print_string "\nImpossible! Veuillez recommencer.\n\n";
    move game ()
in

let game = new_game () in
move game ()
;;
