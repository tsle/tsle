type board = Square.square array array
val row_of_index : int -> int -> int
val index_of_pos : int * int -> int -> int * int
val pos_of_index : int * int -> int -> int * int
val debug_board : Square.square array array -> unit
val print : Square.square array array -> unit
val chessboard : int -> Square.square array array
val map : ('a -> 'b) -> 'a array array -> 'b array array
val exists : ('a -> bool) -> 'a array array -> bool
val filter : ('a -> bool) -> 'a array array -> 'a list
val classic_init : unit -> Square.square array array
val find_square : 'a array array -> int * int -> 'a
val occupied_squares :
  Side.color option -> Square.square array array -> Square.square list
val obstacle :
  Square.square array array ->
  Geometry.position -> Geometry.position -> int * int -> bool
type violation =
    Occupied_destination of Piece.piece
  | Blocked_way of Piece.piece
  | Friendly_fire of Piece.piece
  | Illegal_moving of Piece.piece
  | Illegal_taking of Piece.piece
  | Nothing_to_kill of Piece.piece
  | Too_late_en_passant of Piece.piece
  | Wrong_en_passant_target of Piece.piece
  | King_checked of Piece.piece
  | King_trajectory_attacked of Square.square
  | Rook_not_in_position_for_castling of bool
  | Rook_has_moved of bool
  | No_square_at_rook_castling_destination of Geometry.position
type move_legality =
    Legal of (board -> board) list
  | Illegal of violation list
val legal_moving :
  Piece.piece ->
  Geometry.position -> Geometry.position -> Piece.piece_legality
val reset_pawns_jump_flags :
  Side.color -> Square.square array array -> Square.square array array
val occupy :
  Piece.piece ->
  Geometry.position -> Square.square array array -> Square.square array array
val free :
  Geometry.position -> Square.square array array -> Square.square array array
val move :
  Piece.piece ->
  Geometry.position ->
  Geometry.position -> Square.square array array -> Square.square array array
val pawn_two_squares_jump :
  Piece.piece ->
  Geometry.position ->
  Geometry.position -> Square.square array array -> Square.square array array
val list_of_violations : 'a option list -> 'a list
val legal_castling :
  Square.square array array ->
  Piece.piece ->
  kside:bool -> Square.square -> Square.square -> move_legality
val legal_move :
  Square.square array array ->
  Piece.piece -> Square.square -> Square.square -> move_legality
val check : Square.square array array -> Square.square -> Side.color -> bool
val apply : 'a -> ('a -> 'a) list -> 'a
