type position = int * int
type direction = N | NE | E | SE | S | SW | W | NW
val directions : direction list
val string_of_dir : direction -> string
val string_of_pos : int * int -> string
val string_of_pos_par : int * int -> string
val equal : 'a * 'b -> 'a * 'b -> bool
val direction_vector : int * int -> int * int -> int * int
val vect_of_dir : direction -> int * int
val pgcd : int -> int -> int
val simplify : int * int -> int * int
val dir_of_vect : int * int -> direction
val direction : int * int -> int * int -> direction
val invert : direction -> direction
val next : int -> int * int -> direction -> int * int
val diags : direction -> direction list
