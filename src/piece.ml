open Geometry
open Side

type pawn = {
  just_jumped_two_squares: bool (* true if this pawn last move was the initial two squares jump *)
}

type kind = King | Queen | Rook | Knight | Bishop | Pawn of pawn;;
type piece = {
  kind: kind;
  side: side;
  moved: bool
}

type piece_legality =
| Legal_moving
| Legal_pawn_two_squares_jump
| Legal_taking
| Legal_qside_castling
| Legal_kside_castling
| Illegal
;;

let string_of_legality = function
| Legal_moving -> "Legal_moving"
| Legal_pawn_two_squares_jump -> "Legal_pawn_two_squares_jump"
| Legal_taking -> "Legal_taking"
| Legal_qside_castling -> "Legal_qside_castling"
| Legal_kside_castling -> "Legal_kside_castling"
| Illegal -> "Illegal"
;;

let string_of_kind kind =
  match kind with
  | King -> "Roi"
  | Queen -> "Dame"
  | Rook -> "Tour"
  | Knight -> "Cavalier"
  | Bishop -> "Fou"
  | Pawn _ -> "Pion"
;;

let letter_of_kind kind =
  String.sub (string_of_kind kind) 0 1
;;

let string_of_piece (p:piece) =
  (letter_of_kind p.kind) ^ (letter_of_color p.side.color)
;;

let piece (kind:kind) (side:side): piece =
  { kind = kind; side = side; moved = false }
;;

let pawn just_jumped_two_squares: kind =
  Pawn ({ just_jumped_two_squares = just_jumped_two_squares })
;;

let pawn_init = pawn false;;

let wking = piece King classic_white
and bking = piece King classic_black
and wqueen = piece Queen classic_white
and bqueen = piece Queen classic_black
and wrook = piece Rook classic_white
and brook = piece Rook classic_black
and wknight = piece Knight classic_white
and bknight = piece Knight classic_black
and wbishop = piece Bishop classic_white
and bbishop = piece Bishop classic_black
and wpawn = piece pawn_init classic_white
and bpawn = piece pawn_init classic_black
;;

let classic_position ?(kside = true) (color:color) (kind:kind) =
  match kind, color, kside with
  | King, White, _ -> (5, 1)
  | King, Black, _ -> (5, 8)
  | Queen, White, _ -> (4, 1)
  | Queen, Black, _ -> (4, 8)
  | Rook, White, true -> (8, 1)
  | Rook, White, false -> (1, 1)
  | Rook, Black, true -> (8, 8)
  | Rook, Black, false -> (1, 8)
  | Knight, White, true -> (7, 1)
  | Knight, White, false -> (2, 1)
  | Knight, Black, true -> (7, 8)
  | Knight, Black, false -> (2, 8)
  | Bishop, White, true -> (6, 1)
  | Bishop, White, false -> (3, 1)
  | Bishop, Black, true -> (6, 8)
  | Bishop, Black, false -> (3, 8)
  | Pawn _, _, _ -> invalid_arg("classic_position cannot be applied to pawns.")
;;

let king_castling_dest (kside:bool) (color:color) =
  match color, kside with
  | White, true -> (7, 1)
  | White, false -> (3, 1)
  | Black, true -> (7, 8)
  | Black, false -> (3, 8)
;;

let rook_castling_dest (kside:bool) (color:color) =
  match color, kside with
  | White, true -> (6, 1)
  | White, false -> (4, 1)
  | Black, true -> (6, 8)
  | Black, false -> (4, 8)
;;

let opposing (p:piece) side = p.side <> side;;

let reset piece =
  match piece.kind with
  | Pawn pawn when pawn.just_jumped_two_squares -> { piece with kind = pawn_init }
  | _ -> piece
;;

let legal_vectors (kind:kind):position list =
  match kind with
  | King | Queen -> List.map vect_of_dir directions
  | Rook -> List.map vect_of_dir [N; E; S; W]
  | Knight -> [(1, 2); (2, 1); (2, -1); (1, -2); (-1, -2); (-2, -1); (-2, 1); (-1, 2)]
  | Bishop -> List.map vect_of_dir [NE; SE; SW; NW]
  | _ as other -> invalid_arg("legal_vectors cannot be applied to this kind of piece: " ^ string_of_kind other)
;;

let legal_king_castling_vectors = [(2, 0); (-2, 0)];;

let legal_moving_of_bool (legal_moving:bool) =
  match legal_moving with
  | true -> Legal_moving
  | false -> Illegal
;;

let legal_moving_generic (p:piece) (dir_vect: position) =
  legal_moving_of_bool
    (List.exists (fun v -> v = dir_vect) (legal_vectors p.kind))
;;

let legal_moving_one_shot (p:piece) (orig:position) (dest:position) =
  let dir_vect = direction_vector orig dest in
  legal_moving_generic p dir_vect
;;

let legal_moving_infinite (p:piece) (orig:position) (dest:position) =
  let dir_vect = simplify (direction_vector orig dest) in
  legal_moving_generic p dir_vect
;;

let legal_king_castling (dir_vect:position) =
  List.exists (fun v -> v = dir_vect) legal_king_castling_vectors
;;

let legal_moving_king (p:piece) (orig:position) (dest:position) =
  let dir_vect = direction_vector orig dest in
  let legal_moving = legal_moving_generic p dir_vect
  and legal_castling = legal_king_castling dir_vect in
  match legal_moving, p.moved, legal_castling with
  | Legal_moving, _, _ -> Legal_moving
  | Illegal, false, true ->
    begin match dir_of_vect dir_vect with
    | W -> Legal_qside_castling
    | E -> Legal_kside_castling
    | _ -> Illegal
    end
  | _ -> Illegal
;;

let legal_moving_pawn (p:piece) (orig:position) (dest:position) =
  let fwd = p.side.direction in
  if (dest = next 1 orig fwd) then
    Legal_moving
  else if (not p.moved && (dest = next 2 orig fwd)) then
    Legal_pawn_two_squares_jump
  else
    Illegal
;;

(* Returns true if the move (from orig to dest) is a legal taking move
   for a pawn of the given side *)
let legal_taking_pawn (side:side) (orig:position) (dest:position) =
  match List.exists (fun dir -> (dest = next 1 orig dir)) (forward_diags side) with
  | true -> Legal_taking
  | false -> Illegal
;;

let promotion pawn:piece =
    let () = print_string "Choisissez un type de pièce pour la promotion (D, T, C ou F): " in
    let rec choice () =
      match read_line () with
      | "D" -> { pawn with kind = Queen }
      | "T" -> { pawn with kind = Rook }
      | "C" -> { pawn with kind = Knight }
      | "F" -> { pawn with kind = Bishop }
      | _ -> print_string "Votre choix (D, T, C, ou F): "; choice ()
    in choice ()
;;
